//
//  ViewController.swift
//  GeoMap
//
//  Created by WSR on 03/12/2019.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController,
                    CLLocationManagerDelegate,
                    MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    let manager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.activityType = .fitness
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 1
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.pausesLocationUpdatesAutomatically = true
        return locationManager
    } ()
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        manager.delegate = self
        authorisation()
        // Do any additional setup after loading the view.
    }

func authorisation() {
    if CLLocationManager.authorizationStatus() == .authorizedAlways
        || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        mapView.showsUserLocation = true
    } else {
        manager.requestWhenInUseAuthorization()
    }
    }
}

